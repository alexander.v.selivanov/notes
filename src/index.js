const development = require('./notes/development');
const html = require('./notes/html');
const css = require('./notes/css');
const javascript = require('./notes/javascript');
const react = require('./notes/react');
const node = require('./notes/node');
const git = require('./notes/git');
const database = require('./notes/database');
const mongo = require('./notes/mongo');
const docker = require('./notes/docker');
const dom = require('./notes/dom');
const http = require('./notes/http');
const hardware = require('./notes/hardware');
const linux = require('./notes/linux');

const newPage = '\n++  \n';
const all = development + newPage
	+ html + newPage
	+ css + newPage
	+ javascript + newPage
	+ react + newPage
	+ node + newPage
	//	+ git + newPage
	//	+ database + newPage
	//	+ mongo + newPage
	//	+ docker + newPage
	//	+ dom + newPage
	+ http + newPage
	+ hardware + newPage
	+ linux;

const path = require('path');
const fs = require('fs');
const pdfWriter = require('./pdfWriter');

const outputPath = path.join(__dirname, '../pdf');
if (!fs.existsSync(outputPath)) {
	fs.mkdirSync(outputPath);
}

const getPdfPath = (pdfName) => path.join(outputPath, pdfName + '.pdf');

pdfWriter(development, getPdfPath('development'));
pdfWriter(html, getPdfPath('html'));
pdfWriter(css, getPdfPath('css'));
pdfWriter(javascript, getPdfPath('javascript'));
pdfWriter(react, getPdfPath('react'));
pdfWriter(node, getPdfPath('node'));
pdfWriter(git, getPdfPath('git'));
pdfWriter(database, getPdfPath('database'));
pdfWriter(mongo, getPdfPath('mongo'));
pdfWriter(docker, getPdfPath('docker'));
pdfWriter(dom, getPdfPath('dom'));
pdfWriter(http, getPdfPath('http'));
pdfWriter(hardware, getPdfPath('hardware'));
pdfWriter(linux, getPdfPath('linux'));

pdfWriter(all, getPdfPath('all'));
