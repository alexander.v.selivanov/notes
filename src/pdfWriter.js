const fs = require('fs');
const PDFDocument = require('pdfkit');

const topicFontSize = 12;
const subtopicFontSize = 10;
const minitopicFontSize = 9;
const wordFontSize = 7;
const normalFontSize = 6;

const regularFont = 'Courier';
const boldFont = 'Courier-Bold';
const italicFont = 'Courier-Italic';
const boldItalicFont = 'Courier-BoldItalic';

module.exports = (content, pdfFilePath) => {
    const doc = new PDFDocument({
        margin: 2,
        font: regularFont,
        // 6 inches e-ink reader
        // width = 90 mm, height = 121 mm == approximately B7 (88 x 125)
        size: 'B7',
    });
    doc.fontSize(normalFontSize);

    const outlineRoot = doc.outline;

    doc.regularFont = () => {
        doc.font(regularFont);
        return doc;
    };
    doc.boldFont = () => {
        doc.font(boldFont);
        return doc;
    };
    doc.italicFont = () => {
        doc.font(italicFont);
        return doc;
    };
    doc.boldItalicFont = () => {
        doc.font(boldItalicFont);
        return doc;
    };

    function outline(caption) {
        outlineRoot.addItem(caption);
    }
    
    function topic(str) {
        doc
            .boldFont()
            .fontSize(topicFontSize)
            .text(
                str,
                {
                    align: 'center'
                }
            )
            .fontSize(normalFontSize);
        outline(str);
    }
    function subtopic(str) {
        doc
            .boldFont()
            .fontSize(subtopicFontSize)
            .text(
                str,
                {
                    align: 'center'
                }
            )
            .fontSize(normalFontSize);
        outline(str);
    }
    function minitopic(str) {
        doc
            .boldFont()
            .fontSize(minitopicFontSize)
            .text(str)
            .fontSize(normalFontSize);
    }

    function newPage() {
    	doc.addPage();
    }

    function line() {
        doc
            .moveUp(0.7)
            .regularFont()
            .text('____________________________________________________',
                {
                    align: 'center'
                }
            );
    }

    function link(str) {
        doc
            .italicFont()
            .text(
                str,
                {
                    align: 'justify',
                }
            );    	
    }

    function synonim(str) {
        doc
            .italicFont()
            .text(
                str,
                {
                    indent: 10,
                    align: 'justify',
                }
            );    	
    }

    function text(str) {
        doc
            .regularFont()
            .text(
                str,
                {
                    align: 'left'
                }
            );
    }

	function definition(word, definition) {
        doc
            .boldFont()
            .fontSize(wordFontSize)
            .text(word)
            .fontSize(normalFontSize)
            .regularFont()
            .text(
            	definition,
            	{
            		indent: 10,
                    align: 'justify',
            	}
            )
		
	}

    const reservedSymbols = ['#', '>', '!', '@', '~', '+', '%'];
    const rows = content.split('\n');
    rows.forEach(row => {
		if(row.length > 3){
			if(row[2] === ' ' && row[0] === row[1] && reservedSymbols.some(rs=>rs===row[0])){
				const rowText = row.slice(3);
				switch(row[0]){
					case '#':
						topic(rowText);
						break;		
					case '>':
						subtopic(rowText);
						break;		
					case '!':
						minitopic(rowText);
						break;
								
					case '@':
						link(rowText);
						break;		
					case '~':
						synonim(rowText);
						break;
								
					case '+':
						newPage();
						break;		
					case '%':
						line();
						break;
								
//					case '{':
//						(row);
//						break;		
//					case '}':
//						(row);
//						break;
					default:
						text(row);
				}
			}else{
				const checkDefinition = row.split(' == ');
				if(checkDefinition.length > 1){
					definition(checkDefinition[0],checkDefinition[1]);
				}else{
					text(row);		
				}
			}
		}
    });

    doc.pipe(fs.createWriteStream(pdfFilePath));
    doc.end();
};
