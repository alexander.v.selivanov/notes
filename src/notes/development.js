module.exports = `

## Data structures

Array            Tree        Stack
Linked List      Graph       Queque
Set              Map

## Sort algorithms

!! Simple sorts (n^2)
Insertion sort
Selection sort
!! Efficient sort (n log n)
Merge sort
Heap sort
Quick sort
!! Bubble sort and variants
Bubble sort (n^2)
Shell sort
Comb sort (n^2)
!! Distribution sort
Counting sort (n+r)
Bucket sort (n+k)
Radix sort

## Search algorithms
Linear / Sequential search
Binary / Half-interval / Logarithmic / Binary chop search
Jump search
Interpolation search
Exponential / Doubling / Galloping / Struzik search
Fibonacci search

++ 

## SDLC
!! Software Development Life Cycle
Planning -> Analysis -> Design -> Development -> Integration&Testing -> Implementation -> Maintenance

## Waterfall
-> Requirement Specification
 -> Design
  -> Construction / Implementation / Coding
   -> Integration
    -> Testing / Validation
     -> Installation
      -> Maintenance

## V-Model
    VERIFICATION             |          VALIDATION
%%  
  Requirement Design         |      User Acceptance Test (UAT)
   Functional System Design  |     Systems Test
    Architecture Design      |    Integration Test
     Module Design           |   Unit Test
                      ---  Coding  --- 

## Iteration Model
           Initialization (Initial Planing) 
                        |
       ---------->   Planning -> Requirements
       |                             | 
  Evaluation                 Analysis & Design
       |                             |
  Verification & Testing <-   Implementation
                                     |
                                 Deployment 

## Spiral Model
  1. Plan                   |  2. Risk Analysis
   Requirements, Gathering  |   Risk Reduction, Prototyping
                            |
  --------------------------|-------------------------------
      Realease -> Next loop |
                            |
  4. Evaluate               |  3. Engineering 
   Customer, Evalutation    |   Coding, Testing
 
## Agile Model
            Product Backlog
                   | 
             Sprint Backlog
   ________________|________________
  |       Sprint (1-4 week)         |
  |  Daily Scrum Meeting (24 hours) |
  |_________________________________|________
                   |                         |
              Sprint Review                  |
                   |               Potentially shippable 
              Retrospective          product increment        
                   |
              Sprint planning
              
Sprint Review - demoing the work that was just completed
Sprint Retrospective - identifying areas of improvement to make the next sprint better

!! Manifesto for Agile Software Development
Individuals and Interactions over processes and tools
Working software over comprehensive documentation
Customer Collaboration over contract negotiation
Responding to Change over following a plan
!! 12 Principles
Customer satisfaction by early and continuous delivery of valuable software
Welcome changing requirements, even in late development
Working software is delivered frequently (weeks rather than months)
Close, daily cooperation between business people and developers
Projects are built around motivated individuals, who should be trusted
Face-to-face conversation is the best form of communication (co-location)
Working software is the primary measure of progress
Sustainable development, able to maintain a constant pace
Continuous attention to technical excellence and good design
Simplicity — the art of maximizing the amount of work not done — is essential
Best architectures, requirements, and designs emerge from self-organizing teams
Regularly, the team reflects on how to become more effective and adjusts accordingly

## Extreme programming (XP)
frequent "releases" in short development cycles, which is intended to improve productivity and introduce checkpoints at which new customer requirements can be adopted
programming in pairs or doing extensive code review
unit testing of all code
avoiding programming of features until they are actually needed
a flat management structure
code simplicity and clarity
expecting changes in the customer's requirements as time passes and the problem is better understood, and frequent communication with the customer and among programmers.

++ 

## Object Oriented Programming (OOP)
Abstraction       Polymorphism
Encapsulation     Class
Inheritance       Prototype

!! Covariance
Enables you to use a more derived type than originally specified. You can assign an instance of IEnumerable<Derived> to a variable of type IEnumerable<Base>.
!! Contravariance
Enables you to use a more generic (less derived) type than originally specified. You can assign an instance of Action<Base> to a variable of type Action<Derived>.
!! Invariance
Means that you can use only the type originally specified; so an invariant generic type parameter is neither covariant nor contravariant. You cannot assign an instance of List<Base> to a variable of type List<Derived> or vice versa.

## Functional programming
First-class and higher-order functions
Pure functions
Recursion
Strict versus non-strict evaluation

## General responsibility assingment software patterns or principles (GRASP)
Controller            Low coupling
Creator               Polymorphism
High cohesion         Protected variations
Indirection           Pure fabrication
Information expert

Contoller == a object is a non-user interface object responsible for receiving or handling a system event
Creator == responsible for creating objects (one or more apply: contain or compositely aggregate instances, record or closely use instances, have initializing information for instances)
High cohesion == responsible are strongly related and highly focused
Indirection == assign the responsibility of mediation between two elements to an intermediate object
Information expert == assign the responsibility to the class which has the information necessary to fulfill that responsibility
Low coupling == assign responsibilities to support lower dependency between classes, change in one class having lower impact on other classes, higher reuse potential
Polymorphism == assign a responsibility according to the type of variation of behaviors by polymorphic operations
Protected variations == protects elements from variations on other by wrapping into interface and using polymorphism
Pure fabrication == specially made class to archieve low coupling, high cohesion and reuse potential

## SOLID
Single Responsible Principle (SRP) == a class should have only one reason to change
Open/Closed Principle (OCP) == software entities ... should be open for extension, but closed for modification
Liskov Substitution Principle (LSP) == objects in a program should be replaceable with instances of their subtypes without altering the correctness of that program
Interface Segregation Principle (ISP) == many client-specific interfaces are better than one general-purpose interface
Dependency Inversion Principle (DI) == depend upon abstractions, not concretions

## Domain Driven Design (DDD)
!! Concepts             Building blocks
Context                 Entity
Domain                  Value object
Model                   Aggregate
Ubiquitous language     Domain event
                        Sevice
                        Repository

!! Concepts
Context == The setting in which a word or statement appears that determines its meaning;
Domain == A sphere of knowledge (ontology), influence, or activity. The subject area to which the user applies a program is the domain of the software;
Model == A system of abstractions that describes selected aspects of a domain and can be used to solve problems related to that domain;
Ubiquitous Language == A language structured around the domain model and used by all team members to connect all the activities of the team with the software.

!! Building blocks
Entity == An object that is not defined by its attributes, but rather by a thread of continuity and its identity.
Value object == an object that contains attributes but has no conceptual identity. They should be treated as immutable.
Aggregate == A collection of objects that are bound together by a root entity, otherwise known as an aggregate root. The aggregate root guarantees the consistency of changes being made within the aggregate by forbidding external objects from holding references to its members.
Domain Event == A domain object that defines an event (something that happens). A domain event is an event that domain experts care about.
Service == When an operation does not conceptually belong to any object. Following the natural contours of the problem, you can implement these operations in services.
Repository == Methods for retrieving domain objects should delegate to a specialized Repository object such that alternative implementations may be easy interchanged.

## Service Oriented Architecture (SOA)

!! Manifesto:
- Business value is given more importance than technical strategy.
- Strategic goals are given more importance than project-specific benefits.
- Intrinsic inter-operability is given more importance than custom integration.
- Shared services are given more importance than specific-purpose implementations.
- Flexibility is given more importance than optimization.
- Evolutionary refinement is given more importance than pursuit of initial perfection.

!! Principles
Standardized service contract == Services adhere to a standard communications agreements, as defined collectively by one or more service-description documents within a given set of services.
Service reference autonomy (an aspect of loose coupling) == The relationship between services is minimized to the level that they are only aware of their existence.
Service location transparency (an aspect of loose coupling) == Services can be called from anywhere within the network that it is located no matter where it is present.
Service longevity == Services should be designed to be long lived. Where possible services should avoid forcing consumers to change if they do not require new features, if you call a service today you should be able to call the same service tomorrow.
Service abstraction == The services act as black boxes, that is their inner logic is hidden from the consumers.
Service autonomy == Services are independent and control the functionality they encapsulate, from a Design-time and a run-time perspective.
Service statelessness == Services are stateless, that is either return the requested value or give an exception hence minimizing resource use.
Service granularity == A principle to ensure services have an adequate size and scope. The functionality provided by the service to the user must be relevant.
Service normalization == Services are decomposed or consolidated (normalized) to minimize redundancy. In some, this may not be done, These are the cases where performance optimization, access, and aggregation are required.
Service composability == Services can be used to compose other services.
Service discovery == Services are supplemented with communicative meta data by which they can be effectively discovered and interpreted.
Service reusability == Logic is divided into various services, to promote reuse of code.
Service encapsulation == Many services which were not initially planned under SOA, may get encapsulated or become a part of SOA.

!! Patterns
Each SOA building block can play any of the three roles:
Service provider == It creates a web service and provides its information to the service registry. Each provider debates upon a lot of hows and whys like which service to expose, whom to give more importance: security or easy availability, what price to offer the service for and many more. The provider also has to decide what category the service should be listed in for a given broker service and what sort of trading partner agreements are required to use the service.
Service broker, service registry or service repository == Its main functionality is to make the information regarding the web service available to any potential requester. Whoever implements the broker decides the scope of the broker. Public brokers are available anywhere and everywhere but private brokers are only available to a limited amount of public. UDDI was an early, no longer actively supported attempt to provide Web services discovery.
Service requester/consumer == It locates entries in the broker registry using various find operations and then binds to the service provider in order to invoke one of its web services. Whichever service the service-consumers need, they have to take it into the brokers, bind it with respective service and then use it. They can access multiple services if the service provides multiple services.

## UML (Unified Modeling Language)

!! Structural diagrams
Class diagram                Object diagram
Component diagram            Package diagram
Composite structure diagram  Profile diagram
Deployment diagram
!! Behavioral diagrams
Activity diagram
Communication diagram
Interaction overview diagram
  Sequence diagram
  State diagram
  Timing diagram
Use case diagram

## Software Development Principles
Hollywood priniple == Don't call us, we will call you

KISS - Keep it simple, stupid (Keep it short and simple) == The KISS principle states that most systems work best if they are kept simple rather than made complicated; therefore simplicity should be a key goal in design and unnecessary complexity should be avoided.

YAGNI - You aren't gonna need it == Always implement things when you actually need them, never when you just foresee that you need them.

DRY - Don't repeat yourself == Every piece of knowledge must have a single, unambiguous, authoritative representation within a system

Occam's razor == is the problem-solving principle that the simplest solution tends to be the right one. When presented with competing hypotheses to solve a problem, one should select the solution with the fewest assumptions

++ 

## Software Development Patterns
!! Creatinal
Abstract factory                     Object pool
Builder                              Prototype
Factory method                       Object library
Singleton                            Multiton
Lazy initialization (virtual proxy)  
Resource acquisition is initialization
!! Behavioral
Blackboard                           Observer or Publich/Subscribe
Chain of responsibility              Servant
Command                              Specification
Interpreter                          State
Iterator                             Strategy
Mediator                             Template method
Memento                              Visitor
Null object
!! Structural
Adapter, Wrapper, Translator         Front controller
Bridge                               Marker
Composite                            Module
Decorator                            Proxy
Facade                               Twin
Flyweight
!! Concurrency
Active Object                        Lock
Balking                              Messaging design pattern (MDP)
Binding properties                   Monitor object
Block chain                          Reactor
Double-checked locking               Read-write lock
Event-based asynchronous             Scheduler
Guarded suspension                   Thread pool
Join                                 Thread-specific storage

++ 

!! Creational patterns
Abstract factory == provide an interface for creating families of related or dependent objects without specifying their concrete classes
Builder == separate the construction of a complex object from its representation, allowing the same construction process to create various representations
Factory method == define an interface for creating a single object, but let subclasses decide which class to instantiate. Factory Method lets a class defer instantiation to subclasses (dependency injection)
Lazy initialization (virtual proxy) == tactic of delaying the creation of an object, the calculation of a value, or some other expensive process until the first time it is needed. This pattern appears in the GoF catalog as "virtual proxy", an implementation strategy for the Proxy pattern
Multiton == ensure a class has only named instances, and provide a global point of access to them
Object pool == avoid expensive acquisition and release of resources by recycling objects that are no longer in use. Can be considered a generalisation of connection pool and thread pool patterns
Prototype == specify the kinds of objects to create using a prototypical instance, and create new objects by copying this prototype
Resource acquisition is initialization == ensure that resources are properly released by tying them to the lifespan of suitable objects
Singleton == ensure a class has only one instance, and provide a global point of access to it
Object library == encapsulate object management including factory interface with live and dead lists

!! Structural patterns
Adapter, Wrapper, Translator == convert the interface of a class into another interface clients expect. An adapter lets classes work together that could not otherwise because of incompatible interfaces. The enterprise integration pattern equivalent is the translator
Bridge == decouple an abstraction from its implementation allowing the two to vary independently
Composite == compose objects into tree structures to represent part-whole hierarchies. Composite lets clients treat individual objects and compositions of objects uniformly
Decorator == attach additional responsibilities to an object dynamically keeping the same interface. Decorators provide a flexible alternative to subclassing for extending functionality
Facade == provide a unified interface to a set of interfaces in a subsystem. Facade defines a higher-level interface that makes the subsystem easier to use
Flyweight == use sharing to support large numbers of similar objects efficiently
Front controller == the pattern relates to the design of Web applications. It provides a centralized entry point for handling requests
Marker == empty interface to associate metadata with a class
Module == group several related elements, such as classes, singletons, methods, globally used, into a single conceptual entity
Proxy == provide a surrogate or placeholder for another object to control access to it
Twin == twin allows modeling of multiple inheritance in programming languages that do not support this feature
!! Behavioral patterns
Blackboard == artificial intelligence pattern for combining disparate sources of data (see blackboard system)
Chain of responsibility == avoid coupling the sender of a request to its receiver by giving more than one object a chance to handle the request. Chain the receiving objects and pass the request along the chain until an object handles it
Command == encapsulate a request as an object, thereby letting you parameterize clients with different requests, queue or log requests, and support undoable operations
Interpreter == given a language, define a representation for its grammar along with an interpreter that uses the representation to interpret sentences in the language
Iterator == provide a way to access the elements of an aggregate object sequentially without exposing its underlying representation
Mediator == define an object that encapsulates how a set of objects interact. Mediator promotes loose coupling by keeping objects from referring to each other explicitly, and it lets you vary their interaction independently
Memento == without violating encapsulation, capture and externalize an object's internal state allowing the object to be restored to this state later
Null object == avoid null references by providing a default object
Observer or Publish/Subscribe == define a one-to-many dependency between objects where a state change in one object results in all its dependents being notified and updated automatically
Servant == define common functionality for a group of classes
Specification == recombinable business logic in a Boolean fashion
State == allow an object to alter its behavior when its internal state changes. The object will appear to change its class
Strategy == define a family of algorithms, encapsulate each one, and make them interchangeable. Strategy lets the algorithm vary independently from clients that use it
Template method == define the skeleton of an algorithm in an operation, deferring some steps to subclasses. Template method lets subclasses redefine certain steps of an algorithm without changing the algorithm's structure
Visitor == represent an operation to be performed on the elements of an object structure. Visitor lets you define a new operation without changing the classes of the elements on which it operates

!! Concurrency patterns
Active Object == decouples method execution from method invocation that reside in their own thread of control. The goal is to introduce concurrency, by using asynchronous method invocation and a scheduler for handling requests
Balking == only execute an action on an object when the object is in a particular state
Binding properties == combining multiple observers to force properties in different objects to be synchronized or coordinated in some way
Block chain == decentralized way to store data and agree on ways of processing it in a Merkle tree, optionally using Digital signature for any individual contributions
Double-checked locking == reduce the overhead of acquiring a lock by first testing the locking criterion (the 'lock hint') in an unsafe manner; only if that succeeds does the actual locking logic proceed. Can be unsafe when implemented in some language/hardware combinations. It can therefore sometimes be considered an anti-pattern
Event-based asynchronous == addresses problems with the asynchronous pattern that occur in multithreaded programs
Guarded suspension == manages operations that require both a lock to be acquired and a precondition to be satisfied before the operation can be executed
Join == provides a way to write concurrent, parallel and distributed programs by message passing. Compared to the use of threads and locks, this is a high-level programming model
Lock == one thread puts a "lock" on a resource, preventing other threads from accessing or modifying it
Messaging design pattern (MDP) == allows the interchange of information (i.e. messages) between components and applications
Monitor object == an object whose methods are subject to mutual exclusion, thus preventing multiple objects from erroneously trying to use it at the same time
Reactor == a reactor object provides an asynchronous interface to resources that must be handled synchronously
Read-write lock == allows concurrent read access to an object, but requires exclusive access for write operations
Scheduler == explicitly control when threads may execute single-threaded code
Thread pool == a number of threads are created to perform a number of tasks, which are usually organized in a queue. Typically, there are many more tasks than threads. Can be considered a special case of the object pool pattern
Thread-specific storage == static or "global" memory local to a thread

## Concurency

Mutex == only one owner
Semaphore == counter of owners

## Multitier Architecture

!! Common layers
Presentation layer (UI layer, view layer, presentation tier in multitier architecture)
Application layer (service layer or GRASP Controller Layer)
Business layer (business logic layer (BLL), domain layer)
Data access layer (persistence layer, logging, networking, and other services which are required to support a particular business layer)

!! Three-tier architecture
Three-tier architecture is a client–server software architecture pattern in which the user interface (presentation), functional process logic ("business rules"), computer data storage and data access are developed and maintained as independent modules, most often on separate platforms.
Presentation tier (user interface) == The top-most level of the application is the user interface. The main function of the interface is to translate tasks and results to something the user can understand.
Application tier (business logic, business rules, logic tier, functional process logic, or middle tier) == This layer coordinates the application, processes commands, makes logical decisions and evaluations and performs calculations. It also moves and processes data between the two surrounding layers
Data tier == Here information is stored and retrieved from a database or file system. The information is then passes back to the logic tier for processing, and then eventually back to the user.

## Reactive programming

!! The Reactive Manifesto:
Responsive == focus on providing rapid and consistent response times, establishing reliable upper bounds so they deliver a consistent quality of service
Resilient == stays responsive in the face of failure (achieved by replication, containment, isolation and delegation)
Elastic == The system stays responsive under varying workload by increasing or decreasing the resources allocated to service these inputs
Message Driven == rely on asynchronous message-passing to establish a boundary between components that ensures loose coupling, isolation and location transparency. Employing explicit message-passing enables load management, elasticity, and flow control by shaping and monitoring the message queues in the system and applying back-pressure when necessary. Non-blocking communication allows recipients to only consume resources while active, leading to less system overhead.

## SMART
Specific == target a specific area for improvement
Measurable == quantify or at least suggest an indicator of progress
Assignable == specify who will do it
Realistic == state what results can realistically be achieved, given available resources
Time-related == specify when the result(s) can be achieved.

`;
