module.exports = `
## HTTP Status Codes

!! 1xx Informational
100 Continue             102 Processing (WebDAV)
101 Switching Protocol   103 Early Hints
                             
!! 2xx Success
200 OK                   204 No Content
201 Created              205 Reset Content
202 Accepted             206 Partial Content
203 Non-Authoritative Information 

!! 3xx Redirection
300 Multiple Choice      304 Not Modified
301 Moved Permanently    307 Temporary Redirect
302 Found                308 Permanent Redirect
303 See Other


!! 4xx Client Error
400 Bad Request          414 URI Too Long     
401 Unathorized          415 Unsupported Media Type
403 Forbidden            416 Requested Range Not Satisfiable
404 Not Found            417 Expectation Failed
405 Method Not Allowed   418 I'm a teapot
406 Not Acceptable       422 Unprocessable Entity
407 Proxy Authentication Required
408 Request Timeout      425 Too Early
409 Conflict             426 Upgrade Required
410 Gone                 428 Precondition Required
411 Length Required      429 Too Many Requests
412 Precondition Failed  431 Request Header Fields Too Large
413 Payload Too Large    451 Unavailable For Legal Reasons

!! 5xx Server Error
500 Internal Server Error
501 Not Implemented
502 Bad Gateway
   The server was acting as a gateway or proxy and received an invalid response from the upstream server
503 Service Unavailable
504 Gateway Timeout
505 HTTP Version Not Supported
511 Network Authentication Required

## Representational state transfer (REST)
Client-server architecture    Layered system
Statelessness                 Code on demand
Cacheability                  Uniform interface


GET - read or retrieve data
POST - create a new asset
PUT - update an existing asset
PATCH - partly modify an existing asset
DELETE - delete an existing asset


            Collection             Element
--
GET         List                   Retrieve
PUT         Replace                Replace or Create if not exist
PATCH                              Update
POST        Create new entry
DELETE      Delete collection      Delete entry


`;
