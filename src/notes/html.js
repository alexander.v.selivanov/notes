module.exports = `
## HTML

<header>
  <nav>
  </nav>
</header>
<section>
  <main>

    <article>
      <figure>
        <img />
        <figcaption></figcaption>
      </figure>
    </article>

    <aside>
    </aside>
  
  </main>
</section>
<footer></footer>

a         div         legend     script         
abbr      dl          li         section         
address   dt          link       select         
area      em          main       small         
article   embed       map        source         
aside     fieldset    mark       span         
audio     figcaption  meta       strong         
b         figure      meter      style         
base      footer      nav        sub         
bdi       form        noframes   summary         
bdo       h1          noscript   sup         
quote     h2          object     table         
body      h3          ol         tbody         
br        h4          optgroup   td         
button    h5          option     textarea         
canvas    h6          output     tfoot         
caption   head        p          th         
cite      header      param      thead         
code      hr          pre        time         
col       html        progress   title         
colgroup  i           q          tr         
data      iframe      rp         track         
datalist  img         rt         u         
dd        input       rtc        ul         
del       ins         ruby       var         
details   kbd         s          video         
dfn       label       samp         

!! Base elements
html == Defines the root element of an HTML document
head == Defines a container for a web page's metadata.
link == Defines a link between the current web page and an external link or resource
  href == Defines the URL of the link
  rel == Defines a link type, explaining how the link relates to the current web page (stylesheet, icon, author, next)
  type == Defines the type of the linked resource (text/css, text/html)
meta == Defines metadata attached to a web page
  charset == Defines the character encoding for the whole web page
  http-equiv == Defines meta rules for the web page (Content-Security-Policy, refresh, X-UA-Compatible)
  name == Defines additional information attached to the web page (viewport, theme-color)
  content == Defines the content of the metadata. This varies according to the name or http-equiv value
script == Defines a container for an external script
  src == Defines the source of the external script
  type == Defines the MIME type of the external script (text/javascript)
  async == Allows the external script to be loaded asynchronously
body == The container for a web page's content

!! Forms
form == Defines an interactive form with controls
  action == Defines which URL the form's information is sent to when submitted
  method == Defines the HTTP method used when submitting the form
  name == The form's name when sent to the server. Useful when multiple forms are present on the same web page
  autocomplete == Determines if the browser can autocomplete all the form's controls (off, on)
  target == Defines in which tab or window the clicked link will show up (_blank, _self, _parent, _top)
  enctype == Defines the MIME type of the information sent to the server. Only works if the method is post (application/x-www-form-urlencoded, text/plain, multipart/form-data)
  novalidate == Tells the browser to not validate the form on submission
fieldset == Defines a group of controls within a form
  disabled == Disables the controls the fieldset contains
legend == Defines a caption for a parent's content
button == Defines a clickable button
  name == Defines the unique identifier for that button within the form
  value == The value sent to the server when submitting the form
  type == Defines the button type (submit, reset)
  disabled == Disables the button
  autofocus == Sets focus on the element when the web page loads
label == Defines a label for a form control
  for == Defines which control the label is associated with
input == Defines an interactive control within a web form
  type == Defines the type of form input (text, email, number, checkbox, radio, submit) 
  name == Defines the unique identifier for that input within the form
  placeholder == Defines a non-selectable placeholder text that only appears when the input is empty
  required == Tells the browser that this input is required
  disabled == Disables the input  
textarea == Defines a multi-line text control within a web form
  name == Defines the unique identifier for that textarea within the form
  autocomplete == Determines if the browser can autocomplete the textarea (off, on)
  minlength == Defines the minimum amount of characters the textarea required to be valid
  maxlength == Defines the maxlength amount of characters allowed
  placeholder == Defines a non-selectable placeholder text that only appears when the textarea is empty
  cols == Defines the number of columns
  rows == Defines the number of rows
  wrap == Defines how the text should be wrapped (hard, soft)
  disabled == Disables the textarea
  required == Tells the browser that this textarea is required
  autofocus == Sets focus on the textarea when the web page loads
  readonly == Turns the textarea into a read-only element
  spellcheck == Enables the browser spell checker
  
!! Lists
dl == Defines a definition list
dt == Defines a definition term
dd == Defines an item in a definition list
ol == Defines an ordered list
  type == Defines how the list is numbered (1, a, A, i, I)
  start == Defines a number to start the list with
  reversed == Reverses the order of the list
ul == Defines an unordered list
li == Defines a list item within an ordered list <ol> or unordered list <ul>

!! Semantic
article == Defines a self-contained block of content that can exist in any context. It can have its own header, footer, sections... Useful for a list of blog posts
aside == Defines a block of content that is related to the main content. Displayed as a sidebar usually
section == Defines a section within a web page
nav == Defines a section with navigation links
header == Defines the header of a web page or section
footer == Defines the footer of a web page or section
main == Defines the main content of a web page. Can be displayed with a sidebar
figure == Defines a single self-contained element, usually an image
figcaption == Defines the caption of a <figure>
mark == Defines highlighted text
time == Defines a time on a 24h clock
  datetime == Defines the time and date

!! Tables
table == Defines a container for tabular data
caption == Defines the title of a <table>
thead == Defines a group of table rows <tr> at the start of a <table>
tbody == Defines a group of table rows <tr>
tfoot == Defines a group of table rows <tr> at the end of a <table>
tr == Defines a table row
th == Defines a table header. Must be a direct child of a <tr>
  colspan == Defines how many columns a cell should span across
  rowspan == Defines how many rows a cell should span across
td == Defines a table cell. Must be a direct child of a <tr>
  colspan == Defines how many columns a cell should span across
  rowspan == Defines how many rows a cell should span across


`;
