module.exports = `
pwd == print working directory
df == disk free
du == disk usage
ls == list
cd == 
mkdir == make directory
rmdir == remove empty directory
rm == remove
touch == 
man ==  (--help)
halt == 
reboot == 
cp == copy
mv == move
locate == 
echo == 
cat == 
exit == 
sudo == super user do
su == 
passwd == 
clear == 
tar == 
zip == 
unzip == 
uname == 
ping == 
apt-get == 
apt-cache == 
chmode == 
hostname == 
dmesg == display message / driver message

Ctrl+C == stop command safely
Ctrl+Z == force stop command
`;