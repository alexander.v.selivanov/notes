module.exports = `
## Network
>> OSI (Open Systems Interconnection) model layers
!! Host layers
7. Application (PDU - Data) == High-level APIs, including resource sharing, remote file access
6. Presentation (PDU - Data) == Translation of data between a networking service and an application; including character encoding, data compression and encryption/decryption
5. Session (PDU - Data) == Managing communication sessions, i.e. continuous exchange of information in the form of multiple back-and-forth transmissions between two nodes
4. Transport (PDU - Segment, Datagram) == Reliable transmission of data segments between points on a network, including segmentation, acknowledgement and multiplexing
!! Media layers
3. Network (PDU - Packet) == Structuring and managing a multi-node network, including addressing, routing and traffic control
2. Data link (PDU - Frame) == Reliable transmission of data frames between two nodes connected by a physical layer
1. Physical (PDU - Symbol) == Transmission and reception of raw bit streams over a physical medium
	
`;
