module.exports = `
## React
Component         React.Children
PureComponent     React.Fragment
createElement     React.createRef
createFactory     React.forwardRef
cloneElement
isValidElement

>> ReactDOM
render                 findDOMNode
hydrate                createPortal
unmountComponentAtNode

>> React.Component
constructor        getDerivedStateFromProps 
render             getSnapshotBeforeUpdate
setState           componentDidMount
forceUpdate        componentDidUpdate
defaultProps       componentDidCatch
displayName        shouldComponentUpdate
props              componentWillUnmount
state                   

>> ReactDomServer
renderToString         renderToNodeStream
renderToStaticMarkup   renderToStaticNodeStream

>> React Hooks
useState       useCallback
useEffect      useMemo
useContext     useRef
useReducer     useImperativeHandle
useDebugValue  useLayoutEffect

!! Basic Hooks
useState == returns a statefull value, and a function to update it
useEffect == accepts a function that contains imperative, possibly effectful code
useContext == accepts a context object and returns the current context value for that context

!! Additional Hooks
useReducer == accepts a reducer of type (state, action) => newState, and returns the current state paired with a dispatch method
useCallback == returns a memoized callback
useMemo == returns a memoized value
useRef == returns a mutable ref object whose .current property is initialized to the passed argument
useImperativeHandle == customizes the instance value that is exposed to parent components when using ref
useLayoutEffect == the signature is indentical to useEffect, but it fires synchronously after all DOM mutations
useDebugValue == can be used to display a label for custom hooks in React DevTools

!! Types

React.FunctionComponent<P> or React.FC<P> == Type representing a functional component
React.Component<P, S> == Type representing a class component
React.ComponentProps<typeof Component> == Gets type of Component Props, so you don't need to export Props from your component ever! (Works for both FC and Class components)
  type MyComponentProps = React.ComponentProps<typeof MyComponent>;
React.ComponentType<P> == Type representing union type of (React.FC | React.Component)
React.ReactElement or JSX.Element == Type representing a concept of React Element - representation of a native DOM component (e.g. <div />), or a user-defined composite component (e.g. <MyComponent />)
React.ReactNode == Type representing any possible type of React node (basically ReactElement (including Fragments and Portals) + primitive JS types)
React.CSSProperties == Type representing style object in JSX (useful for css-in-js styles)
React.ReactEventHandler<E> == Type representing generic event handler
React.MouseEvent<E> | React.KeyboardEvent<E> | React.TouchEvent<E> etc... == Type representing more specific event handler

## React Router

## Redux

## Redux Saga

`;