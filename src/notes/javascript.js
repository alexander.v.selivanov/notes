module.exports = `
## JavaScript Types
!! Primitive type (value)
string, number, boolean, null, undefined, symbol
!! Complex type (reference)
object, array, function
>> JavaScript Coercion
- Objects evaluate to true
- Undefined evaluates to false
- Null evaluates to false
- Booleans evaluate to the value of the boolean
- Numbers evaluate to false if +0, -0, or NaN, otherwise true
- Strings evaluate to false if an empty string '', otherwise true
>> JavaScript Build in values and objects
!! Value properties
Infinity, NaN, undefined, null
!! Fundamental objects
Object, Function, Boolean, Symbol, Error, EvalError, InternalError, RangeError, ReferenceError, SyntaxError, TypeError, URIError
!! Numbers and dates
Number, Math, Date
!! Text processing
String, RegExp
!! Indexed collections
Array, Int8Array, Uint8Array, Uint8ClampedArray, Int16Array, Uint16Array, Int32Array, Uint32Array, Float32Array, Float64Array
!! Keyed collections
Map, Set, WeakMap, WeakSet
!! Structured data
ArrayBuffer, SharedArrayBuffer, Atomics, DataView, JSON
!! Control abstraction objects
Promise, Generator, GeneratorFunction, AsyncFunction
!! Reflection
Reflect, Proxy
!! Internationalization
Intl, Intl.Collator, Intl.DateTimeFormat, Intl.NumberFormat

## JavaScript Globals
!! Properties
Infinity (infinity number = Number.POSITIVE_INFINITY), NaN (Not-A-Number)
!! Functions
isFinite (passed value is a finite number, use Number.isFinite), isNaN (value is NaN, use Number.isNaN), parseFloat (return a floating point), parseInt (return an integer of the specified radix/base)

## JavaScript Boolean
Boolean(if the value is omitted or is 0, -0, null, false, NaN, undefined, "").valueOf() = false
valueOf() == return the primitive boolean value

## JavaScript Number
!! Properties    Class methods Object methods
EPSILON          isNaN         toExponential
MAX_SAFE_INTEGER isFinite      toFixed
MAX_VALUE        isInteger     toLocaleString
MIN_SAFE_INTEGER isSafeInteger toPrecision
MIN_VALUE        parseFloat    toString
NaN              parseInt
NEGATIVE_INFINITY
POSITIVE_INFINITY

!! Properties
EPSILON == smallest interval between two numbers
MAX_SAFE_INTEGER == the maximum safe integer (2^53-1)
MAX_VALUE == the largest positive number
MIN_SAFE_INTEGER == the minimum safe integer (-(2^53-1))
MIN_VALUE == the smallest positive number (closest to zero)
NaN == not a number
NEGATIVE_INFINITY == negative infinity (returned on overflow)
POSITIVE_INFINITY == infinity (returned on overflow)
!! Class methods
isNaN == argument is NaN
isFinite == argument is finite
isInteger == argument is an integer
isSafeInteger == argument is a safe integer -(2^53-1) < x < 2^53-1
parseFloat == return a floating point
parseInt == return an integer of the specified radix
!! Object methods
toExponential == return a string in exponential notation
toFixed == return a string in fixed-point notation
toLocaleString == return a language sensitive string value
toPrecision == return a string to a specified precission in fixed-point or exponential notation 
toString == return a string in the specified radix/base
valueOf == return the primitive value

## JavaScript Object
!! Class methods          Class methods         Object methods
assign                    getOwnPropertySymbols hasOwnProperty
create                    getPrototypeOf        isPrototypeOf
defineProperty            is                    propertyIsEnumerable
defineProperties          isExtensible          toLocaleString
entries                   isFrozen              toString
freeze                    isSealed              valueOf
fromEntries               keys                       
getOwnPropertyDescriptor  preventExtensions        
getOwnPropertyDescriptors seal                       
getOwnPropertyNames       setPrototypeOf             
values

!! Properties
Object.length == has a value of 1.

!! Methods
Object.assign() == copies the values of all enumerable own properties from one or more source objects to a target object.
Object.create() == creates a new object with the specified prototype object and properties. Object.create(null) - set no inherited properties (no prototype).
Object.defineProperty(obj, prop, descriptor) == adds the named property described by a given descriptor to an object. The descriptor for the property being defined or modified:
 - configurable - true if and only if the type of this property descriptor may be changed and if the property may be deleted from the corresponding object. Defaults to false.
 - enumerable - true if and only if this property shows up during enumeration of the properties on the corresponding object. Defaults to false.
 - value - the value associated with the property. Can be any valid JavaScript value. Defaults to undefined.
 - writable - true if and only if the value associated with the property may be changed with an assignment operator. Defaults to false.
 - get - a function which serves as a getter for the property, or undefined if there is no getter. Defaults to undefined.
 - set - a function which serves as a setter for the property, or undefined if there is no setter. Defaults to undefined.
Object.defineProperties() == adds the named properties described by the given descriptors to an object.
Object.entries() == returns an array containing all of the [key, value] pairs of a given object's own enumerable string properties.
Object.freeze() == freezes an object: other code can't delete or change any properties.
Object.fromEntries() == returns a new object from an iterable of key-value pairs (reverses Object.entries).
Object.getOwnPropertyDescriptor() == returns a property descriptor for a named property on an object.
Object.getOwnPropertyDescriptors() == returns an object containing all own property descriptors for an object.
Object.getOwnPropertyNames() == returns an array containing the names of all of the given object's own enumerable and non-enumerable properties.
Object.getOwnPropertySymbols() == returns an array of all symbol properties found directly upon a given object.
Object.getPrototypeOf() == returns the prototype of the specified object.
Object.is() == compares if two values are the same value. Equates all NaN values (which differs from both Abstract Equality Comparison and Strict Equality Comparison).
Object.isExtensible() == determines if extending of an object is allowed.
Object.isFrozen() == determines if an object was frozen.
Object.isSealed() == determines if an object is sealed.
Object.keys() == returns an array containing the names of all of the given object's own enumerable string properties.
Object.preventExtensions() == prevents any extensions of an object.
Object.seal() == prevents other code from deleting properties of an object.
Object.setPrototypeOf() == sets the prototype (i.e., the internal [[Prototype]] property).
Object.values() == returns an array containing the values that correspond to all of a given object's own enumerable string properties.

hasOwnProperty() == returns a boolean indicating whether an object contains the specified property as a direct property of that object and not inherited through the prototype chain.
isPrototypeOf() == returns a boolean indicating whether the object this method is called upon is in the prototype chain of the specified object.
propertyIsEnumerable() == returns a boolean indicating if the internal ECMAScript [[Enumerable]] attribute is set.
toLocaleString() == calls toString().
toString() == returns a string representation of the object.
valueOf() == returns the primitive value of the specified object.

## JavaScript Array
!! Mutator methods  Accessor methods Iteration methods
copyWithin          concat           entries
fill                includes         every
pop                 indexOf          filter
push                join             find
reverse             lastIndexOf      findIndex
shift               slice            forEach
sort                                 keys
splice                               map
unshift                              reduce 
                                     reduceRight
                                     some
                                     values

!! Mutator methods
copyWithin == copies a sequence of array elements within the array
fill == fills all the elements of an array from a start index to an end index with a static value
pop == removes the last element from an array and returns that element
push == adds one or more elements to the end of an array and returns the new length of the array
reverse == reverses the order of the elements of an array in place — the first becomes the last, and the last becomes the first
shift == removes the first element from an array and returns that element
sort == sorts the elements of an array in place and returns the array
splice == adds and/or removes elements from an array
unshift == adds one or more elements to the front of an array and returns the new length of the array

!! Accessor methods
concat == returns a new array comprised of this array joined with other array(s) and/or value(s)
includes == determines whether an array contains a certain element, returning true or false as appropriate
indexOf == returns the first (least) index of an element within the array equal to the specified value, or -1 if none is found
join == joins all elements of an array into a string
lastIndexOf == returns the last (greatest) index of an element within the array equal to the specified value, or -1 if none is found
slice == extracts a section of an array and returns a new array

!! Iteration methods
entries == returns a new Array Iterator object that contains the key/value pairs for each index in the array
every == returns true if every element in this array satisfies the provided testing function
filter == creates a new array with all of the elements of this array for which the provided filtering function returns true
find == returns the found value in the array, if an element in the array satisfies the provided testing function or undefined if not found
findIndex == returns the found index in the array, if an element in the array satisfies the provided testing function or -1 if not found
forEach == calls a function for each element in the array
keys == returns a new Array Iterator that contains the keys for each index in the array
map == creates a new array with the results of calling a provided function on every element in this array
reduce(callback(accumulator, currentValue[,index[,array]]), initialValue) == apply a function against an accumulator and each value of the array (from left-to-right) as to reduce it to a single value
reduceRight == apply a function against an accumulator and each value of the array (from right-to-left) as to reduce it to a single value
some == returns true if at least one element in this array satisfied the provided testing function
values == returns a new Array Iterator object that contains the values for each index in the array (same to Array.prototype[@@iterator]())

## JavaScript String
length                   localeCompare()     substring()
String.fromCharCode()    match()             toLocaleLowerCase()
String.fromCodePoint()   normalize()         toLocaleUpperCase()
charAt()                 padEnd()            toLowerCase()
charCodeAt()             padStart()          toString()
codePointAt()            repeat()            toUpperCase()
concat()                 replace()           trim()
includes()               search()            trimStart(), trimLeft()
endsWith()               slice()             trimEnd(), trimRight()
indexOf()                split()             valueOf()
lastIndexOf()            startsWith()        String.prototype[@@iterator]()

!! Properties
length == reflects the length of the string

!! Methods
String.fromCharCode() == returns a string created by using the specified sequence of Unicode values.
String.fromCodePoint() == returns a string created by using the specified sequence of code points.

charAt() == returns the character (exactly one UTF-16 code unit) at the specified index.
charCodeAt() == returns a number that is the UTF-16 code unit value at the given index.
codePointAt() == returns a non negative integer Number that is the code point value of the UTF-16 encoded code point starting at the specified index.
concat() == combines the text of two strings and returns a new string.
includes() == determines whether one string may be found within another string.
endsWith() == determines whether a string ends with the characters of another string.
indexOf() == returns the index within the calling String object of the first occurrence of the specified value, or -1 if not found.
lastIndexOf() == returns the index within the calling String object of the last occurrence of the specified value, or -1 if not found.
localeCompare() == returns a number indicating whether a reference string comes before or after or is the same as the given string in sort order.
match() == used to match a regular expression against a string.
normalize() == returns the Unicode Normalization Form of the calling string value.
padEnd() == pads the current string from the end with a given string to create a new string from a given length.
padStart() == pads the current string from the start with a given string to create a new string from a given length.
repeat() == returns a string consisting of the elements of the object repeated the given times.
replace() == used to find a match between a regular expression and a string, and to replace the matched substring with a new substring.
search() == executes the search for a match between a regular expression and a specified string.
slice() == extracts a section of a string and returns a new string.
split() == splits a String object into an array of strings by separating the string into substrings.
startsWith() == determines whether a string begins with the characters of another string.
substring() == returns the characters in a string between two indexes into the string.
toLocaleLowerCase() == the characters within a string are converted to lower case while respecting the current locale. For most languages, this will return the same as toLowerCase().
toLocaleUpperCase() == the characters within a string are converted to upper case while respecting the current locale. For most languages, this will return the same as toUpperCase().
toLowerCase() == returns the calling string value converted to lower case.
toString() == returns a string representing the specified object. Overrides the Object.prototype.toString() method.
toUpperCase() == returns the calling string value converted to uppercase.
trim() == trims whitespace from the beginning and end of the string. Part of the ECMAScript 5 standard.
trimStart(), trimLeft() == trims whitespace from the beginning of the string.
trimEnd(), trimRight() == trims whitespace from the end of the string.
valueOf() == returns the primitive value of the specified object. Overrides the Object.prototype.valueOf() method.
String.prototype[@@iterator]()    Returns a new Iterator object that iterates over the code points of a String value, returning each code point as a String value.

## JavaScript Error
!! Error types
EvalError          SyntaxError
RangeError         TypeError
ReferenceError     URIError

## JavaScript RegExp
/pattern/flags
new RegExp(pattern[, flags])
RegExp(pattern[, flags])

!! flags
g == global match; find all matches rather than stopping after the first match
i == ignore case; if u flag is also enabled, use Unicode case folding
m == multiline; treat beginning and end characters (^ and $) as working over multiple lines (i.e., match the beginning or end of each line (delimited by \n or \r), not only the very beginning or end of the whole input string)
s == "dotAll"; allows . to match newlines
u == Unicode; treat pattern as a sequence of Unicode code points
y == sticky; matches only from the index indicated by the lastIndex property of this regular expression in the target string (and does not attempt to match from any later indexes).

RegExp.lastIndex == The index at which to start the next match.

!! Properties
flags == A string that contains the flags of the RegExp object.
dotAll == Whether . matches newlines or not.
global == Whether to test the regular expression against all possible matches in a string, or only against the first.
ignoreCase == Whether to ignore case while attempting a match in a string.
multiline == Whether or not to search in strings across multiple lines.
source == The text of the pattern.
sticky == Whether or not the search is sticky.
unicode == Whether or not Unicode features are enabled.

!! Methods
exec() == Executes a search for a match in its string parameter.
test() == Tests for a match in its string parameter.
RegExp.prototype[@@match]() == Performs match to given string and returns match result.
RegExp.prototype[@@matchAll]() == Returns all matches of the regular expression against a string.
RegExp.prototype[@@replace]() == Replaces matches in given string with new substring.
RegExp.prototype[@@search]() == Searches the match in given string and returns the index the pattern found in the string.
RegExp.prototype[@@split]() == Splits given string into an array by separating the string into substring.
toString() == Returns a string representing the specified object. Overrides the Object.prototype.toString() method.

## JavaScript Function

!! Properties
arguments == An array corresponding to the arguments passed to a function. This is deprecated as property of Function. Use the arguments object available within the function instead.
length == Specifies the number of arguments expected by the function.
name == The name of the function.
constructor == Specifies the function that creates an object's prototype. See Object.prototype.constructor for more details.

!! Methods
apply() == Calls a function and sets its this to the provided value, arguments can be passed as an Array object.
bind() == Creates a new function which, when called, has its this set to the provided value, with a given sequence of arguments preceding any provided when the new function was called.
call() == Calls (executes) a function and sets its this to the provided value, arguments can be passed as they are.
toString() == Returns a string representing the source code of the function. Overrides the Object.prototype.toString method.

## JavaScript Generator
The Generator object is returned by a generator function and it conforms to both the iterable protocol and the iterator protocol.

!! Methods
next() == Returns a value yielded by the yield expression.
return() == Returns the given value and finishes the generator.
throw() == Throws an error to a generator (also finishes the generator, unless caught from within that generator).

## JavaScript Date
Creates a JavaScript Date instance that represents a single moment in time in a platform-independent format. Date objects use a Unix Time Stamp, an integer value that is the number of milliseconds since 1 January 1970 UTC.

new Date();
new Date(value);
new Date(dateString);
new Date(year, monthIndex [, day [, hours [, minutes [, seconds [, milliseconds]]]]]);

!! Methods
Date.now() == Returns the numeric value corresponding to the current time - the number of milliseconds elapsed since January 1, 1970 00:00:00 UTC, with leap seconds ignored.
Date.parse() == Parses a string representation of a date and returns the number of milliseconds since 1 January, 1970, 00:00:00, UTC, with leap seconds ignored.
Note: Parsing of strings with Date.parse is strongly discouraged due to browser differences and inconsistencies.
Date.UTC() == Accepts the same parameters as the longest form of the constructor (i.e. 2 to 7) and returns the number of milliseconds since January 1, 1970, 00:00:00 UTC, with leap seconds ignored.

!! Getter
getDate() == Returns the day of the month (1-31) for the specified date according to local time.
getDay() == Returns the day of the week (0-6) for the specified date according to local time.
getFullYear() == Returns the year (4 digits for 4-digit years) of the specified date according to local time.
getHours() == Returns the hour (0-23) in the specified date according to local time.
getMilliseconds() == Returns the milliseconds (0-999) in the specified date according to local time.
getMinutes() == Returns the minutes (0-59) in the specified date according to local time.
getMonth() == Returns the month (0-11) in the specified date according to local time.
getSeconds() == Returns the seconds (0-59) in the specified date according to local time.
getTime() == Returns the numeric value of the specified date as the number of milliseconds since January 1, 1970, 00:00:00 UTC (negative for prior times).
getTimezoneOffset() == Returns the time-zone offset in minutes for the current locale.
getUTCDate() == Returns the day (date) of the month (1-31) in the specified date according to universal time.
getUTCDay() == Returns the day of the week (0-6) in the specified date according to universal time.
getUTCFullYear() == Returns the year (4 digits for 4-digit years) in the specified date according to universal time.
getUTCHours() == Returns the hours (0-23) in the specified date according to universal time.
getUTCMilliseconds() == Returns the milliseconds (0-999) in the specified date according to universal time.
getUTCMinutes() == Returns the minutes (0-59) in the specified date according to universal time.
getUTCMonth() == Returns the month (0-11) in the specified date according to universal time.
getUTCSeconds() == Returns the seconds (0-59) in the specified date according to universal time.
getYear()  == Returns the year (usually 2-3 digits) in the specified date according to local time. Use getFullYear() instead.
!! Setter
setDate() == Sets the day of the month for a specified date according to local time.
setFullYear() == Sets the full year (e.g. 4 digits for 4-digit years) for a specified date according to local time.
setHours() == Sets the hours for a specified date according to local time.
setMilliseconds() == Sets the milliseconds for a specified date according to local time.
setMinutes() == Sets the minutes for a specified date according to local time.
setMonth() == Sets the month for a specified date according to local time.
setSeconds() == Sets the seconds for a specified date according to local time.
setTime() == Sets the Date object to the time represented by a number of milliseconds since January 1, 1970, 00:00:00 UTC, allowing for negative numbers for times prior.
setUTCDate() == Sets the day of the month for a specified date according to universal time.
setUTCFullYear() == Sets the full year (e.g. 4 digits for 4-digit years) for a specified date according to universal time.
setUTCHours() == Sets the hour for a specified date according to universal time.
setUTCMilliseconds() == Sets the milliseconds for a specified date according to universal time.
setUTCMinutes() == Sets the minutes for a specified date according to universal time.
setUTCMonth() == Sets the month for a specified date according to universal time.
setUTCSeconds() == Sets the seconds for a specified date according to universal time.
setYear()  == Sets the year (usually 2-3 digits) for a specified date according to local time. Use setFullYear() instead.
!! Conversion getter
toDateString() == Returns the "date" portion of the Date as a human-readable string like 'Thu Apr 12 2018'
toISOString() == Converts a date to a string following the ISO 8601 Extended Format.
toJSON() == Returns a string representing the Date using toISOString(). Intended for use by JSON.stringify().
toGMTString() == Returns a string representing the Date based on the GMT (UT) time zone. Use toUTCString() instead.
toLocaleDateString() == Returns a string with a locality sensitive representation of the date portion of this date based on system settings.
toLocaleFormat() == Converts a date to a string, using a format string.
toLocaleString() == Returns a string with a locality sensitive representation of this date. Overrides the Object.prototype.toLocaleString() method.
toLocaleTimeString() == Returns a string with a locality sensitive representation of the time portion of this date based on system settings.
toSource() == Returns a string representing the source for an equivalent Date object; you can use this value to create a new object. Overrides the Object.prototype.toSource() method.
toString() == Returns a string representing the specified Date object. Overrides the Object.prototype.toString() method.
toTimeString() == Returns the "time" portion of the Date as a human-readable string.
toUTCString() == Converts a date to a string using the UTC timezone.
valueOf() == Returns the primitive value of a Date object. Overrides the Object.prototype.valueOf() method.

## JavaScript Error

!! Parameters
message == A human-readable description of the error (Optional)

!! Error types
EvalError == Creates an instance representing an error that occurs regarding the global function eval().
RangeError == Creates an instance representing an error that occurs when a numeric variable or parameter is outside of its valid range.
ReferenceError == Creates an instance representing an error that occurs when de-referencing an invalid reference.
SyntaxError == Creates an instance representing a syntax error that occurs while parsing code in eval().
TypeError == Creates an instance representing an error that occurs when a variable or parameter is not of a valid type.
URIError == Creates an instance representing an error that occurs when encodeURI() or decodeURI() are passed invalid parameters.

## JavaScript JSON
JSON.parse() == Parse a string as JSON, optionally transform the produced value and its properties, and return the value.
JSON.stringify() == Return a JSON string corresponding to the specified value, optionally including only certain properties or replacing property values in a user-defined manner.

## JavaScript Set

!! Properties
size == Returns the number of values in the Set object.

!! Methods
add(value) == Appends a new element with the given value to the Set object. Returns the Set object.
clear() == Removes all elements from the Set object.
delete(value) == Removes the element associated to the value and returns the value that has(value) would have previously returned. has(value) will return false afterwards.
entries() == Returns a new Iterator object that contains an array of [value, value] for each element in the Set object, in insertion order. This is kept similar to the Map object, so that each entry has the same value for its key and value here.
forEach(callbackFn[, thisArg]) == Calls callbackFn once for each value present in the Set object, in insertion order. If a thisArg parameter is provided to forEach, it will be used as the this value for each callback.
has(value) == Returns a boolean asserting whether an element is present with the given value in the Set object or not.
keys() == Is the same function as the values() function and returns a new Iterator object that contains the values for each element in the Set object in insertion order.
values() == Returns a new Iterator object that contains the values for each element in the Set object in insertion order.
Set.prototype[@@iterator]() == Returns a new Iterator object that contains the values for each element in the Set object in insertion order.

## JavaScript Map

!! Properties
size == Returns the number of key/value pairs in the Map object.
!! Methods
clear() == Removes all key/value pairs from the Map object.
delete(key) == Returns true if an element in the Map object existed and has been removed, or false if the element does not exist. has(key) will return false afterwards.
entries() == Returns a new Iterator object that contains an array of [key, value] for each element in the Map object in insertion order.
forEach(callbackFn[, thisArg]) == Calls callbackFn once for each key-value pair present in the Map object, in insertion order. If a thisArg parameter is provided to forEach, it will be used as the this value for each callback.
get(key) == Returns the value associated to the key, or undefined if there is none.
has(key) == Returns a boolean asserting whether a value has been associated to the key in the Map object or not.
keys() == Returns a new Iterator object that contains the keys for each element in the Map object in insertion order.
set(key, value) == Sets the value for the key in the Map object. Returns the Map object.
values() == Returns a new Iterator object that contains the values for each element in the Map object in insertion order.
Map.prototype[@@iterator]() == Returns a new Iterator object that contains an array of [key, value] for each element in the Map object in insertion order.

## JavaScript Math
!! Properties
Math.E == Euler's constant and the base of natural logarithms, approximately 2.718.
Math.LN2 == Natural logarithm of 2, approximately 0.693.
Math.LN10 == Natural logarithm of 10, approximately 2.303.
Math.LOG2E == Base 2 logarithm of E, approximately 1.443.
Math.LOG10E == Base 10 logarithm of E, approximately 0.434.
Math.PI == Ratio of the circumference of a circle to its diameter, approximately 3.14159.
Math.SQRT1_2 == Square root of 1/2; equivalently, 1 over the square root of 2, approximately 0.707.
Math.SQRT2 == Square root of 2, approximately 1.414.

!! Methods
Math.abs(x) == Returns the absolute value of a number.
Math.acos(x) == Returns the arccosine of a number.
Math.acosh(x) == Returns the hyperbolic arccosine of a number.
Math.asin(x) == Returns the arcsine of a number.
Math.asinh(x) == Returns the hyperbolic arcsine of a number.
Math.atan(x) == Returns the arctangent of a number.
Math.atanh(x) == Returns the hyperbolic arctangent of a number.
Math.atan2(y, x) == Returns the arctangent of the quotient of its arguments.
Math.cbrt(x) == Returns the cube root of a number.
Math.ceil(x) == Returns the smallest integer greater than or equal to a number.
Math.clz32(x) == Returns the number of leading zeroes of a 32-bit integer.
Math.cos(x) == Returns the cosine of a number.
Math.cosh(x) == Returns the hyperbolic cosine of a number.
Math.exp(x) == Returns Ex, where x is the argument, and E is Euler's constant (2.718…), the base of the natural logarithm.
Math.expm1(x) == Returns subtracting 1 from exp(x).
Math.floor(x) == Returns the largest integer less than or equal to a number.
Math.fround(x) == Returns the nearest single precision float representation of a number.
Math.hypot([x[, y[, …]]]) == Returns the square root of the sum of squares of its arguments.
Math.imul(x, y) == Returns the result of a 32-bit integer multiplication.
Math.log(x) == Returns the natural logarithm (loge, also ln) of a number.
Math.log1p(x) == Returns the natural logarithm (loge, also ln) of 1 + x for a number x.
Math.log10(x) == Returns the base 10 logarithm of a number.
Math.log2(x) == Returns the base 2 logarithm of a number.
Math.max([x[, y[, …]]]) == Returns the largest of zero or more numbers.
Math.min([x[, y[, …]]]) == Returns the smallest of zero or more numbers.
Math.pow(x, y) == Returns base to the exponent power, that is, base exponent.
Math.random() == Returns a pseudo-random number between 0 and 1.
Math.round(x) == Returns the value of a number rounded to the nearest integer.
Math.sign(x) == Returns the sign of the x, indicating whether x is positive, negative or zero.
Math.sin(x) == Returns the sine of a number.
Math.sinh(x) == Returns the hyperbolic sine of a number.
Math.sqrt(x) == Returns the positive square root of a number.
Math.tan(x) == Returns the tangent of a number.
Math.tanh(x) == Returns the hyperbolic tangent of a number.
Math.trunc(x) == Returns the integer part of the number x, removing any fractional digits.

## JavaScript Promise

!! Methods
Promise.all(iterable) == Returns a promise that either fulfills when all of the promises in the iterable argument have fulfilled or rejects as soon as one of the promises in the iterable argument rejects. If the returned promise fulfills, it is fulfilled with an array of the values from the fulfilled promises in the same order as defined in the iterable. If the returned promise rejects, it is rejected with the reason from the first promise in the iterable that rejected. This method can be useful for aggregating results of multiple promises.
Promise.race(iterable) == Returns a promise that fulfills or rejects as soon as one of the promises in the iterable fulfills or rejects, with the value or reason from that promise.
Promise.reject() == Returns a Promise object that is rejected with the given reason.
Promise.resolve() == Returns a Promise object that is resolved with the given value. If the value is a thenable (i.e. has a then method), the returned promise will "follow" that thenable, adopting its eventual state; otherwise the returned promise will be fulfilled with the value. Generally, if you don't know if a value is a promise or not, Promise.resolve(value) it instead and work with the return value as a promise.

## JavaScript Proxy
The Proxy object is used to define custom behavior for fundamental operations

!! Terminology
handler == Placeholder object which contains traps.
traps == The methods that provide property access. This is analogous to the concept of traps in operating systems.
target == Object which the proxy virtualizes. It is often used as storage backend for the proxy. Invariants (semantics that remain unchanged) regarding object non-extensibility or non-configurable properties are verified against the target.

new Proxy(target, handler);
target == A target object to wrap with Proxy. It can be any sort of object, including a native array, a function or even another proxy.
handler == An object whose properties are functions which define the behavior of the proxy when an operation is performed on it.

!! Methods
Proxy.revocable() == Creates a revocable Proxy object.

!! Methods of the handler object
The handler object is a placeholder object which contains traps for Proxy.
All traps are optional. If a trap has not been defined, the default behavior is to forward the operation to the target.

handler.getPrototypeOf() == A trap for Object.getPrototypeOf.
handler.setPrototypeOf() == A trap for Object.setPrototypeOf.
handler.isExtensible() == A trap for Object.isExtensible.
handler.preventExtensions() == A trap for Object.preventExtensions.
handler.getOwnPropertyDescriptor() == A trap for Object.getOwnPropertyDescriptor.
handler.defineProperty() == A trap for Object.defineProperty.
handler.has() == A trap for the in operator.
handler.get() == A trap for getting property values.
handler.set() == A trap for setting property values.
handler.deleteProperty() == A trap for the delete operator.
handler.ownKeys() == A trap for Object.getOwnPropertyNames and Object.getOwnPropertySymbols.
handler.apply() == A trap for a function call.
handler.construct() == A trap for the new operator.

## JavaScript Intl

!! Properties
Intl.Collator == Constructor for collators, which are objects that enable language-sensitive string comparison.
Intl.DateTimeFormat == Constructor for objects that enable language-sensitive date and time formatting.
Intl.ListFormat == Constructor for objects that enable language-sensitive list formatting.
Intl.NumberFormat == Constructor for objects that enable language-sensitive number formatting.
Intl.PluralRules == Constructor for objects that enable plural-sensitive formatting and language-specific rules for plurals.
Intl.RelativeTimeFormat == Constructor for objects that enable language-sensitive relative time formatting.
!! Methods
Intl.getCanonicalLocales() == Returns canonical locale names.

## JavaScript ArrayBuffer
The ArrayBuffer object is used to represent a generic, fixed-length raw binary data buffer.
It is an array of bytes, often refered to in other languages as a "byte array".
You cannot directly manipulate the contents of an ArrayBuffer; instead, you create one of the typed array objects or a DataView object which represents the buffer in a specific format, and use that to read and write the contents of the buffer.

new ArrayBuffer(length)
length == The size, in bytes, of the array buffer to create.

!! Methods
ArrayBuffer.isView(arg) == Returns true if arg is one of the ArrayBuffer views, such as typed array objects or a DataView. Returns false otherwise.

!! Properties
byteLength == The size, in bytes, of the array. This is established when the array is constructed and cannot be changed. Read only.
!! Methods
slice() == Returns a new ArrayBuffer whose contents are a copy of this ArrayBuffer's bytes from begin, inclusive, up to end, exclusive. If either begin or end is negative, it refers to an index from the end of the array, as opposed to from the beginning.

## JavaScript DataView
The DataView view provides a low-level interface for reading and writing multiple number types in a binary ArrayBuffer, without having to care about the platform's endianness.

new DataView(buffer [, byteOffset [, byteLength]])
buffer == An existing ArrayBuffer or SharedArrayBuffer  to use as the storage backing the new DataView object.
byteOffset == The offset, in bytes, to the first byte in the above buffer for the new view to reference. If unspecified, the buffer view starts with the first byte.
byteLength == The number of elements in the byte array. If unspecified, the view's length will match the buffer's length.

!! Properties
buffer == The ArrayBuffer referenced by this view. Fixed at construction time and thus read only.
byteLength == The length (in bytes) of this view from the start of its ArrayBuffer. Fixed at construction time and thus read only.
byteOffset == The offset (in bytes) of this view from the start of its ArrayBuffer. Fixed at construction time and thus read only.
!! Methods Read
getInt8() == Gets a signed 8-bit integer (byte) at the specified byte offset from the start of the view.
getUint8() == Gets an unsigned 8-bit integer (unsigned byte) at the specified byte offset from the start of the view.
getInt16() == Gets a signed 16-bit integer (short) at the specified byte offset from the start of the view.
getUint16() == Gets an unsigned 16-bit integer (unsigned short) at the specified byte offset from the start of the view.
getInt32() == Gets a signed 32-bit integer (long) at the specified byte offset from the start of the view.
getUint32() == Gets an unsigned 32-bit integer (unsigned long) at the specified byte offset from the start of the view.
getFloat32() == Gets a signed 32-bit float (float) at the specified byte offset from the start of the view.
getFloat64() == Gets a signed 64-bit float (double) at the specified byte offset from the start of the view.
!! Method Write
setInt8() == Stores a signed 8-bit integer (byte) value at the specified byte offset from the start of the view.
setUint8() == Stores an unsigned 8-bit integer (unsigned byte) value at the specified byte offset from the start of the view.
setInt16() == Stores a signed 16-bit integer (short) value at the specified byte offset from the start of the view.
setUint16() == Stores an unsigned 16-bit integer (unsigned short) value at the specified byte offset from the start of the view.
setInt32() == Stores a signed 32-bit integer (long) value at the specified byte offset from the start of the view.
setUint32() == Stores an unsigned 32-bit integer (unsigned long) value at the specified byte offset from the start of the view.
setFloat32() == Stores a signed 32-bit float (float) value at the specified byte offset from the start of the view.
setFloat64() == Stores a signed 64-bit float (double) value at the specified byte offset from the start of the view.

## JavaScript SharedArrayBuffer
The SharedArrayBuffer object is used to represent a generic, fixed-length raw binary data buffer, similar to the ArrayBuffer object, but in a way that they can be used to create views on shared memory. Unlike an ArrayBuffer, a SharedArrayBuffer cannot become detached.

new SharedArrayBuffer(length)
length == The size, in bytes, of the array buffer to create

!! Properties
byteLength == The size, in bytes, of the array. This is established when the array is constructed and cannot be changed. Read only.
!! Methods
slice(begin, end) == Returns a new SharedArrayBuffer whose contents are a copy of this SharedArrayBuffer's bytes from begin, inclusive, up to end, exclusive. If either begin or end is negative, it refers to an index from the end of the array, as opposed to from the beginning.

## JavaScript Atomics
The Atomics object provides atomic operations as static methods. They are used with SharedArrayBuffer objects.

!! Methods Atomic operations
When memory is shared, multiple threads can read and write the same data in memory. Atomic operations make sure that predictable values are written and read, that operations are finished before the next operation starts and that operations are not interrupted.
Atomics.add() == Adds the provided value to the existing value at the specified index of the array. Returns the old value at that index.
Atomics.and() == Computes a bitwise AND on the value at the specified index of the array with the provided value. Returns the old value at that index.
Atomics.compareExchange() == Stores a value at the specified index of the array, if it equals a value. Returns the old value.
Atomics.exchange() == Stores a value at the specified index of the array. Returns the old value.
Atomics.load() == Returns the value at the specified index of the array.
Atomics.or() == Computes a bitwise OR on the value at the specified index of the array with the provided value. Returns the old value at that index.
Atomics.store() == Stores a value at the specified index of the array. Returns the value.
Atomics.sub() == Subtracts a value at the specified index of the array. Returns the old value at that index.
Atomics.xor() == Computes a bitwise XOR on the value at the specified index of the array with the provided value. Returns the old value at that index.

!! Methods Wait and notifySection
The wait() and notify() methods are modeled on Linux futexes ("fast user-space mutex") and provide ways for waiting until a certain condition becomes true and are typically used as blocking constructs.
Atomics.wait() == Verifies that the specified index of the array still contains a value and sleeps awaiting or times out. Returns either "ok", "not-equal", or "timed-out". If waiting is not allowed in the calling agent then it throws an Error exception (most browsers will not allow wait() on the browser's main thread).
Atomics.notify() == Notifies agents that are waiting on the specified index of the array. Returns the number of agents that were notified.
Atomics.isLockFree(size) == An optimization primitive that can be used to determine whether to use locks or atomic operations. Returns true, if an atomic operation on arrays of the given element size will be implemented using a hardware atomic operation (as opposed to a lock). Experts only.

`;
