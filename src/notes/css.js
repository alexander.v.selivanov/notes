module.exports = `

## CSS Properties

align-content              clear                margin-right
align-items                color                margin-top
align-self                 column-count         margin
animation-delay            column-gap           max-height
animation-direction        column-width         max-width
animation-duration         content              min-height
animation-fill-mode        cursor               min-width
animation-iteration-count  display              mix-blend-mode
animation-name             flex-basis           opacity
animation-play-state       flex-direction       order
animation-timing-function  flex-flow            outline-color
animation                  flex-grow            outline-style
background-attachment      flex-shrink          outline-width
background-clip            flex-wrap            outline
background-color           float                overflow-wrap
background-image           font-family          overflow-x
background-origin          font-size            overflow-y
background-position        font-style           overflow 
background-repeat          font-variant         padding-bottom
background-size            font-weight          padding-left
background                 font                 padding-right
border-bottom-color        grid-area            padding-top
border-bottom-left-radius  grid-auto-columns    padding
border-bottom-right-radius grid-auto-flow       pointer-events
border-bottom-style        grid-auto-rows       position
border-bottom-width        grid-column-end      resize
border-bottom              grid-column-gap      right
border-collapse            grid-column-start    text-align 
border-color               grid-column          text-decoration
border-left-color          grid-gap             text-indent
border-left-style          grid-row-end         text-overflow
border-left-width          grid-row-gap         text-shadow
border-left                grid-row-start       text-transform
border-radius              grid-row             top
border-right-color         grid-template-areas  transform-origin
border-right-style         grid-template-column transform
border-right-width         grid-template-rows   box-shadow
border-right               grid-template        box-sizing
border-style               grid                 vertical-align
border-top-color           height               white-space
border-top-left-radius     justify-content      width
border-top-right-radius    left                 will-change
border-top-style           letter-spacing       word-break
border-top-width           line-height          word-spacing
border-top                 list-style-image     z-index
border-width               list-style-position  bottom
border                     list-style-type
transition-delay           list-style
transition-duration        margin-bottom
transition-property        margin-left
transition-timing-function
transition

!! Animations
animation == Shorthand property for animation-name animation-duration animation-timing-function animation-delay animation-iteration-count animation-direction animation-fill-mode and animation-play-state. Only animation-duration and animation-name are required
animation-delay == Defines how long the animation has to wait before starting. The animation will only be delayed on its first iteration
animation-direction == Defines in which direction the animation is played (normal, reverse, alternate, alternate-reverse)
animation-duration == Defines how long the animation lasts
animation-fill-mode == Defines what happens before an animation starts and after it ends. The fill mode allows to tell the browser if the animation’s styles should also be applied outside of the animation (none, forwards, backwards, both)
animation-iteration-count == Defines how many times the animation is played
animation-name == Defines which animation keyframes to use
animation-play-state == Defines if an animation is playing or not (running, paused)
animation-timing-function == Defines how the values between the start and the end of the animation are calculated (ease, ease-in, ease-out, ease-in-out, linear, step-start, step-end, steps)

!! Backgrounds
background == Shorthand property for background-image background-position background-size background-repeat background-origin background-clip background-attachment and background-color.
background-attachment == Defines how the background image will behave when scrolling the page (scroll, fixed)
background-clip == Defines how far the background should extend within the element (border-box, padding-box, content-box)
background-color == Defines the color of the element's background (transparent, rgb, rgba, hsl, hsla)
background-image == Defines an image as the background of the element (none, url, linear-gradient, radial-gradient)
background-origin == Defines the origin of the background image (padding-box, border-box, content-box)
background-position == Defines the position of the background image
background-repeat == Defines how the background image repeats itself across the element's background, starting from the background position (repeat, repeat-x, repeat-y, no-repeat)
background-size == Defines the size of the background image (auto, contain, cover)

!! Box model
border-width == Defines the width of the element's borders
  border-bottom-width == for the bottom border only
  border-left-width == for the left border only
  border-right-width == for the right border only
  border-top-width == for the top border only
box-sizing == Defines how the width and height of the element are calculated: whether they include the padding and borders or not (content-box, border-box)
line-height == Defines the height of a single line of text
margin == Shorthand property for margin-top margin-right margin-bottom and margin-left
  margin-bottom == Defines the space outside the element, on the bottom side
  margin-left == Defines the space outside the element, on the left side
  margin-right == Defines the space outside the element, on the right side
  margin-top == Defines the space outside the element, on the top side
max-height == Defines the maximum height the element can be
max-width == Defines the maximum width the element can be
min-height == Defines the minimum height of the element
min-width == Defines the minimum width of the element
padding == Shorthand property for padding-top padding-right padding-bottom and padding-left
padding-bottom == Defines the space inside the element, on the bottom side
padding-left == Defines the space inside the element, on the left side
padding-right == Defines the space inside the element, on the right side
padding-top == Defines the space inside the element, on the top side
height == Defines the height of the element
width == Defines the width of the element

!! Flexbox
align-content == Defines how each line is aligned within a flexbox/grid container. It only applies if flex-wrap: wrap is present, and if there are multiple lines of flexbox/grid items (stretch, flex-start, flex-end, center, space-between, space-around)
align-items == Defines how flexbox items are aligned according to the cross axis, within a line of a flexbox container (flex-start, flex-end, center, baseline, stretch)
align-self == Works like align-items, but applies only to a single flexbox item, instead of all of them (auto, flex-start, flex-end, center, baseline, stretch)
flex-basis == Defines the initial size of a flexbox item
flex-grow == Defines how much a flexbox item should grow if there's space available
flex-flow == Shorthand property for flex-direction and flex-wrap
flex-direction == Defines how flexbox items are ordered within a flexbox container (row, row-reverse, column, column-reverse)
flex-wrap == Defines if flexbox items appear on a single line or on multiple lines within a flexbox container (nowrap, wrap, wrap-reverse)
flex-shrink == Defines how much a flexbox item should shrink if there's not enough space available
justify-content == Defines how flexbox/grid items are aligned according to the main axis, within a flexbox/grid container (flex-start, flex-end, center, space-between,space-around)
order == Defines the order of a flexbox item

!! Grid
grid == Shorthand property for grid-template-rows grid-template-columns grid-template-areas grid-auto-rows grid-auto-columns and grid-auto-flow
grid-area == Shorthand property for grid-row-start grid-column-start grid-row-end and grid-column-end.
grid-auto-columns == Defines the size of grid columns that were created implicitly: it means that grid-auto-columns targets the columns that were not defined with grid-template-columns or grid-template-areas
grid-auto-flow == Defines the position of auto-generated grid items (row, column)
grid-auto-rows == Defines the size of grid rows that were created implicitly: it means that grid-auto-rows targets the rows that were not defined with grid-template-rows or grid-template-areas
grid-column ==  Shorthand property for grid-column-start and grid-column-end
grid-column-start == Defines the column start position of a grid item
grid-column-end == Defines the column end position of a grid item
grid-gap == Shorthand property for grid-row-gap and grid-column-gap
grid-row-gap == Defines the gutter between the rows of a grid container
grid-column-gap == Defines the gutter between the columns of a grid container
grid-row == Shorthand property for grid-row-start and grid-row-end
grid-row-start == Defines the row start position of a grid item
grid-row-end == Defines the row end position of a grid item
grid-template == Shorthand property for grid-template-rows grid-template-columns and grid-template-area
  grid-template-areas == Defines areas within a grid container. These areas can then be referenced when placing a grid item
  grid-template-columns == Defines the columns of a grid container
  grid-template-rows == Defines the rows of a grid container

!! Positioning
position == Defines the position behavior of the element
  - static - (default) remain in the natural flow of the page (ignore: top, bottom, left, right, z-index)
  - relative - remain in the natural flow of the page (can be used with: top, bottom, left, right, z-index)
  - absolute - not remain in the natural flow of the page. It will position itself according to the closest positioned ancestor (can be used with: top, bottom, left, right, z-index)
  - fixed - not remain in the natural flow of the page. It will position itself according to the viewport (can be used with: top, bottom, left, right, z-index)
  - sticky - positioned 'relative' until a given offset position is met in the viewport -then it 'sticks' in place like 'fixed' (basec on th user's scroll position)
  - inherit - inherited from a parent element 
top == Defines the position of the element according to its top edge
bottom == Defines the position of the element according to its bottom edge
right == Defines the position of the element according to its right edge
left == Defines the position of the element according to its left edge
z-index == Defines the order of the elements on the z-axis. It only works on positioned elements (anything apart from static)

!! Transitions
transition == Shorthand property for transition-property transition-duration transition-timing-function and transition-delay
transition-delay == Defines how long the transition has to wait before starting
transition-duration == Defines how long the transition lasts
transition-property == Defines which properties will transition (all, none, background, color, transform)
transition-timing-function == Defines how the values between the start and the end of the transition are calculated (ease, ease-in, ease-out, ease-in-out, linear, step-start, step-end, steps))

!! Typography
color == Defines the color of the text (transparent, rgb, rgba, hsl, hsla)
font == Shorthand property for font-style font-variant font-weight font-size line-height and font-family.
font-family == When using multiple values, the font-family list of font families defines the priority in which the browser should choose the font family
font-size == Defines the size of the text (larger, smaller, xx-small, x-small, small, medium, large, x-large, xx-large)
font-style == Defines how much the text is slanted (normal, italic, oblique)
font-variant == Defines which glyph to use for each letter (normal, small-caps)
font-weight == Defines the weight of the text (normal, bold, lighter, bolder, 100 Thin, 200 Extra Light, 300 Light, 400 Normal, 500 Medium, 600 Semi Bold, 700 Bold, 800 Extra Bold, 900 Ultra Bold)
letter-spacing == Defines the spacing between the characters of a block of text
line-height == Defines the height of a single line of text
text-align == Defines how the text content of the element is horizontally aligned (left, right, center, justify)
text-decoration == Defines how the text content of the element is decorated (none, underline)
text-indent == Defines the indentation of the element's first line of text
text-overflow == Defines how the hidden text content behaves if it's overflowing (clip, ellipsis)
text-shadow == Defines the shadow of the text content
text-transform == Defines how the text content should be transformed (none, capitalize, uppercase, lowercase)
white-space == Defines how the element's white space is handled (normal, nowrap, pre, pre-wrap, pre-line)
word-break == Defines how words should break when reaching the end of a line (normal, break-all)
word-spacing == Defines the spacing between words of a block of text

!! Display
inline == an inline element (height and width properties will have no effect)
block == a block element (starts on a new line and takes up the whole width)
contents == makes the container disappear, making th child elements children of the element the next level up in the DOM
flex == a block-level flex container
grid == a block-level grid container
inline-block == an inline-level block container (formatted as an inline element, but height and width can be applied)
inline-flex == an inline-level flex container
inline-grid == an inline-level grid container
inline-table == an inline-level table
list-item == behave like a <li> element
run-in == either block or inline, depending on context
table == behave like a <table> element
table-caption == behave like a <caption> element
table-column-group == behave like a <colgroup> element
table-header-group == behave like a <thead> element
table-footer-group == behave like a <tfoot> element
table-row-group == behave like a <tbody> element
table-cell == behave like a <td> element
table-column == behave like a <col> element
table-row == behave like a <tr> element
none == completely removed
intial == set property to its default value
inherit == inherits this property from its parent element

## CSS Selectors
.class == Selects all elements with class="intro"
#id == Selects the element with id="firstname"
* == Selects all elements
element == Selects all <p> elements
element,element == Selects all <div> elements and all <p> elements
element element == Selects all <p> elements inside <div> elements
element>element == Selects all <p> elements where the parent is a <div> element
element+element == Selects all <p> elements that are placed immediately after <div> elements
element1~element2 == Selects every <ul> element that are preceded by a <p> element
[attribute] == Selects all elements with a target attribute
[attribute=value] == Selects all elements with target="_blank"
[attribute~=value] == Selects all elements with a title attribute containing the word "flower"
[attribute|=value] == Selects all elements with a lang attribute value starting with "en"
[attribute^=value] == Selects every <a> element whose href attribute value begins with "https"
[attribute$=value] == Selects every <a> element whose href attribute value ends with ".pdf"
[attribute*=value] == Selects every <a> element whose href attribute value contains the substring "w3schools"
:active == Selects the active link
::after == Insert something after the content of each <p> element
::before == Insert something before the content of each <p> element
:checked == Selects every checked <input> element
:default == Selects the default <input> element
:disabled == Selects every disabled <input> element
:empty == Selects every <p> element that has no children (including text nodes)
:enabled == Selects every enabled <input> element
:first-child == Selects every <p> element that is the first child of its parent
::first-letter == Selects the first letter of every <p> element
::first-line == Selects the first line of every <p> element
:first-of-type == Selects every <p> element that is the first <p> element of its parent
:focus == Selects the input element which has focus
:hover == Selects links on mouse over
:in-range == Selects input elements with a value within a specified range
:indeterminate == Selects input elements that are in an indeterminate state
:invalid == Selects all input elements with an invalid value
:lang(language) == Selects every <p> element with a lang attribute equal to "it" (Italian)
:last-child == Selects every <p> element that is the last child of its parent
:last-of-type == Selects every <p> element that is the last <p> element of its parent
:link == Selects all unvisited links
:not(selector) == Selects every element that is not a <p> element
:nth-child(n) == Selects every <p> element that is the second child of its parent
:nth-last-child(n) == Selects every <p> element that is the second child of its parent, counting from the last child
:nth-last-of-type(n) == Selects every <p> element that is the second <p> element of its parent, counting from the last child
:nth-of-type(n) == Selects every <p> element that is the second <p> element of its parent
:only-of-type == Selects every <p> element that is the only <p> element of its parent
:only-child == Selects every <p> element that is the only child of its parent
:optional == Selects input elements with no "required" attribute
:out-of-range == Selects input elements with a value outside a specified range
::placeholder == Selects input elements with placeholder text
:read-only == Selects input elements with the "readonly" attribute specified
:read-write == Selects input elements with the "readonly" attribute NOT specified
:required == Selects input elements with the "required" attribute specified
:root == Selects the document's root element
::selection == Selects the portion of an element that is selected by a user
:target == Selects the current active #news element (clicked on a URL containing that anchor name)
:valid == Selects all input elements with a valid value
:visited == Selects all visited links

## CSS Functions
attr() == Returns the value of an attribute of the selected element
calc() == Allows you to perform calculations to determine CSS property values
cubic-bezier() == Defines a Cubic Bezier curve
hsl() == Defines colors using the Hue-Saturation-Lightness model (HSL)
hsla() == Defines colors using the Hue-Saturation-Lightness-Alpha model (HSLA)
linear-gradient() == Sets a linear gradient as the background image. Define at least two colors (top to bottom)
radial-gradient() == Sets a radial gradient as the background image. Define at least two colors (center to edges)
repeating-linear-gradient() == Repeats a linear gradient
repeating-radial-gradient() == Repeats a radial gradient
rgb() == Defines colors using the Red-Green-Blue model (RGB)
rgba() == Defines colors using the Red-Green-Blue-Alpha model (RGBA)
var() == Inserts the value of a custom property

## CSS Units

!! Absolute Lengths
cm == centimeters
mm == millimeters
in == inches (1in = 96px = 2.54cm)
px == pixels (1px = 1/96th of 1in)
pt == points (1pt = 1/72 of 1in)
pc == picas (1pc = 12 pt)

!! Relative Lengths
em == Relative to the font-size of the element (2em means 2 times the size of the current font)
ex == Relative to the x-height of the current font (rarely used)
ch == Relative to width of the "0" (zero)
rem == Relative to font-size of the root element
vw == Relative to 1% of the width of the viewport
vh == Relative to 1% of the height of the viewport
vmin == Relative to 1% of viewport's smaller dimension
vmax == Relative to 1% of viewport's larger dimension
% == Relative to the parent element

`;
